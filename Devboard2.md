# Devboard 2.0

In order to improve Devboard 1.0 and correct some of its flaws, FST Lisboa decided to create a new version of it: Devboard 2.0. 
One of the main objectives is to make the serial interface more reliable and to do so it was decided to use the [FT260S-U][ftdi].
This chip implements full speed usb, a baudrate matching usb in the interface protocol and prevents the pic33ep256mu806 from implementing
the usb firmware, which affected negatively the performance of the rest of the code.
To make this new version of the Devboard with even more functionalities it was added indicators and inputs such as [LEDs][led] and [buttons][button].

[ftdi]:https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT260.pdf
[led]:https://pt.mouser.com/datasheet/2/348/smlvn6rgb1w-e-1139143.pdf
[button]:https://pt.mouser.com/ProductDetail/CK/KMS231GPLFS?qs=sGAEpiMZZMsgGjVA3toVBDaywH8eIqckU3CRc3V6fnA%3d

## Requirements:
- Altium Designer
- libraries: dev_lib, telemetria and FSTLib. 
- Pacience
- João Freitas

## Main changes:
- LED updated;
- 2 right angle buttons added;
- FT260S-U [28-Pin TSSOP] added;
- Secondary oscillator deleted;
- LDO footprint corrected: TAB connected to 3.3V;
- Direct USB-PIC connection deleted, so USBCL6 componnent was also deleted.
- USB port updated from telemetria library (this new version contains mechanical supports) 

## Rules:
- Hole size: 0.4mm or 0.3mm;
- Tracks without square angles;
- PGND and GND don't need tracks at the beginning. Only trace them after pour the polygons;
- Tracks should be kept away from the edge;
- DCDC hole size > 0.85mm;
- Power tracks: 24V, 5V, 5V_USB and 3.3V should have a thickness of at least 0.4mm (more thickness equalts to less resistance);
- USB tracks (D+ and D-) should be as shorter as possible and should be kept together. Avoid using pads. If it is really necessary, 
use an even number of vias and both signals (D+ and D-) should have the same number of vias.
- PIC capacitators are [decoupling capacitors][dc] and so they should be linked as directly as possible to power tracks. Each decoupling
capacitator should be connected to a single power track. 

[dc]:https://en.wikipedia.org/wiki/Decoupling_capacitor





